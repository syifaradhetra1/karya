<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tulis extends Model
{
    use HasFactory;

    protected $table = "tulis";
    protected $primarykey = 'id_tulis';
    protected $fillable = ['id_user', 'judul_tulis', 'nama_user', 'email', 'file_tulis', 'status'];
    public $timestamps = false;
}
