<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sastra extends Model
{
    use HasFactory;

    protected $table = "sastra";
    protected $primarykey = 'id_sastra';
    protected $fillable = ['id_user', 'judul_sastra', 'nama_user', 'email', 'file_sastra', 'status'];
    public $timestamps = false;
}
