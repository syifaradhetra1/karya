<?php

namespace App\Http\Controllers;

use App\Models\Tulis;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Barryvdh\DomPDF\Facade\Pdf as FacadePdf;
use PDF;

class TulisController extends Controller
{
    // Admin
    public function daftarKaryaTulis(){
        $daftarKaryaTulis = DB::table('tulis')
                                ->select('*')
                                ->get();
        
        return view('admin.tulis', ['daftarKaryaTulis'=> $daftarKaryaTulis]);
    }

    public function detailKaryaTulis($tulis){
        $getuser = DB::table('users')
                        ->select('*')
                        ->where('users.nama_user', '=', session()->get('nama_user'))
                        ->get();

        $detailKaryaTulis = DB::table('tulis')
                            ->leftJoin('users', 'tulis.id_user', '=', 'users.id_user')
                            ->select('tulis.*', 'users.id_user')
                            ->where('tulis.id_tulis', $tulis)
                            ->get();

        return view('admin.detailtulis', ['detailKaryaTulis'=> $detailKaryaTulis]);
    }

    public function admEditTulis($idtulis){
        $dataTulis = DB::table('tulis')
                        ->select('*')
                        ->where('tulis.id_tulis', $idtulis)
                        ->get();
                        // dd($dataTulis);       
        return view('admin.edittulis', ['idtulis'=> $dataTulis]);
    }

    public function admUpdateTulis(Request $request, $id){
        $request->validate([            
            'status' => 'required'
        ]);

        Tulis::where('id_tulis', $id)
                ->update([                   
                    'status' => $request->status
                ]);
        return redirect('admin/tulis')->with('succes', 'Karya Tulis Berhasil Diupdate');
    }

    //User
    public function daftarTulis(){
        $getuser = DB::table('users')
                        ->select('*')
                        ->where('users.nama_user', '=', session()->get('nama_user'))
                        ->get();
        
        $daftarTulis = DB::table('tulis')
                                ->leftJoin('users', 'tulis.id_user', '=', 'users.id_user')
                                // ->leftJoin('admin', 'tulis.id_admin', '=', 'admin.id_admin')
                                ->select('tulis.*', 'users.id_user')
                                ->where('tulis.id_user', $getuser[0]->id_user)
                                ->get();
                                // dd($daftarTulis);
        
        return view('user/tulis', ['daftarTulis'=> $daftarTulis]);
    }

    public function createTulis(){
        // $tulis = Tulis::all();
        $getuser = DB::table('users')
                        ->select('*')
                        ->where('users.nama_user', '=', session()->get('nama_user'))
                        ->get();
        // $users = User::all();
        return view('user.tambahtulis', ['getuser'=>$getuser]);
    }

    public function tambahTulis(Request $request){
        // dd($request->all());
        $request->validate([
            // 'id_tulis' => 'required',
            'id_user' => 'required',
            'judul_tulis' => 'required',
            'nama_user' => 'required',
            'email' => 'required',
            'file_tulis' => 'required|mimes:pdf|max:10048',
            // 'status' => 'required'
        ]);
        // dd($request->hasFile('file_tulis'));
        if ($request->hasFile('file_tulis')) {
            $request->validate([
                'file_tulis' => 'required|mimes:pdf|max:10048',
            ]);

            $file= $request->file('file_tulis');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(public_path('/file_tulis'), $filename);

        }
        // dd($filename);

        $daftarTulis = new Tulis([
            // 'id_tulis' => '',
            'id_user' => $request->id_user,
            'judul_tulis' => $request->judul_tulis,
            'nama_user' => $request->nama_user,
            'email' => $request->email,
            'file_tulis' => $filename,
            // 'status' => $request->status
        ]);
        $daftarTulis->save();

        return redirect('/user/tulis')->with('succes', 'Data Karya Tulis Berhasil Ditambahkan !!!');
    }

    public function detailKaryaTulisUser($tulis){
        $getuser = DB::table('users')
                        ->select('*')
                        ->where('users.nama_user', '=', session()->get('nama_user'))
                        ->get();

        $detailKaryaTulis = DB::table('tulis')
                            ->leftJoin('users', 'tulis.id_user', '=', 'users.id_user')
                            ->select('tulis.*', 'users.id_user')
                            ->where('tulis.id_tulis', $tulis)
                            ->get();

        return view('user.detailtulis', ['detailKaryaTulis'=> $detailKaryaTulis]);
    }

    public function deleteTulis($tulis){
        //delete old gambar
        // dd($tulis);
            $oldfile_tulis = DB::table('tulis')
                                ->select('tulis.*')
                                ->where('id_tulis', $tulis)
                                ->get();
            // dd($oldfile_tulis);
            $oldfilepath = public_path('file_tulis\\'.$oldfile_tulis[0]->file_tulis);
            if(File::exists($oldfilepath)){
                File::delete($oldfilepath);
            }

        // Tulis::destroy('id_tulis',$tulis);
        DB::table('tulis')
            ->where('id_tulis',$tulis)
            ->delete();
        return redirect('/user/tulis')->with('success', 'Karya Tulis Berhasil di hapus!');
    }
}
