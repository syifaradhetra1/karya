<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    //
    public function dashboardAdmin(){
        //karya tulis
        $jumlahkaryatulis = DB::table('tulis')
                            ->select('*')
                            // ->where('status', 'Disetujui')
                            ->get();
        
        //karya sastra
        $jumlahkaryasastra = DB::table('sastra')
                            ->select('*')
                            // ->where('status', 'Disetujui')
                            ->get();

        return view('admin.index', ['jumlahkaryatulis'=> $jumlahkaryatulis, 'jumlahkaryasastra'=> $jumlahkaryasastra]);
    }

    public function dashboardUser(){
        // $iduserlogin = session()->get('id_user');
        
        //karya sastra
        $getuser = DB::table('users')
                        ->select('*')
                        ->where('users.nama_user', '=', session()->get('nama_user'))
                        ->get();
                        // dd($getuser[0]->id_user);
        $jumkaryasastra = DB::table('sastra')
                            // ->leftJoin('users', 'sastra.id_user', '=', 'users.id_user')
                            ->select('*')
                            ->where('sastra.id_user', '=', $getuser[0]->id_user)
                            ->get();
        
        //karya tulis
        $jumkaryatulis = DB::table('tulis')
                            // ->leftJoin('users', 'tulis.id_user', '=', 'users.id_user')
                            ->select('tulis.id_user')
                            ->where('tulis.id_user', '=', $getuser[0]->id_user )
                            ->get();

        return view('user.index', ['jumkaryatulis'=> $jumkaryatulis, 'jumkaryasastra'=>$jumkaryasastra, 'getuser'=> $getuser]);
    }
}
