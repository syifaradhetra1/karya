<?php

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Http\Request;

class AkunUserController extends Controller
{
    //
    public function showLogin(){
        return view('user.login');
    }
    public function userLogin(Request $request){
        $request->validate([
            'username'=>'required',
            'password'=>'required',
        ]);

        $dataloguser = Users::where('username', $request->username)
                                ->where('password', $request->password)
                                ->get();
        // dd(count($dataloguser)<1);
        if(count($dataloguser)<1){
            // return response()->json('disiini');
            
            return redirect('user/login')->with('pesanlogin', 'Periksa Username dan Password Anda !');
        }else{
            $request->session()->put('nama_user',$dataloguser[0]['nama_user']);
            // return response()->json($datalogadmin[0]['nama_admin']);
            return redirect('user/index')->with('pesanlogin', 'Anda Berhasil Login Sebagai User');


        }
    }


    public function userLogout(Request $request){
        $request->session()->forget('nama_user');
        return redirect('user/login')->with('logedout', 'Anda telah log out dari User');
        // return response()->json('admin berhasil logout');
    }
}
