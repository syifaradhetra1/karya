<?php

namespace App\Http\Controllers;

use App\Models\Sastra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


class SastraController extends Controller
{
    //admin
    public function daftarKaryaSastra(){
        $daftarKaryaSastra = DB::table('sastra')
                                ->select('*')
                                ->get();
        
        return view('admin.sastra', ['daftarKaryaSastra'=> $daftarKaryaSastra]);
    }

    public function detailKaryaSastra($sastra){
        $detailKaryaSastra = DB::table('sastra')
                            ->leftJoin('users', 'sastra.id_user', '=', 'users.id_user')
                            ->select('sastra.*', 'users.id_user')
                            ->where('sastra.id_sastra', $sastra)
                            ->get();

        return view('admin.detailsastra', ['detailKaryaSastra'=> $detailKaryaSastra]);
    }

    public function admEditSastra($idsastra){
        $dataSastra = DB::table('sastra')
                        ->select('*')
                        ->where('sastra.id_sastra', $idsastra)
                        ->get();
                        // dd($dataTulis);
        
        
        return view('admin.editsastra', ['idsastra'=> $dataSastra]);
    }

    public function admUpdateSastra(Request $request, $id){
        $request->validate([
            
            'status' => 'required'
        ]);

        Sastra::where('id_sastra', $id)
                ->update([
                   
                    'status' => $request->status
                ]);
        return redirect('admin/sastra')->with('succes', 'Karya Sastra Berhasil Diupdate');
    }

    //user
    public function daftarSastra(){
        $getuser = DB::table('users')
                        ->select('*')
                        ->where('users.nama_user', '=', session()->get('nama_user'))
                        ->get();
        
        $daftarSastra = DB::table('sastra')
                                ->leftJoin('users', 'sastra.id_user', '=', 'users.id_user')
                                // ->leftJoin('admin', 'sastra.id_admin', '=', 'admin.id_admin')
                                ->select('sastra.*', 'users.id_user')
                                ->where('sastra.id_user', $getuser[0]->id_user)
                                ->get();
                                // dd($daftarSastra);
        
        return view('user/sastra', ['daftarSastra'=> $daftarSastra]);
    }

    public function createSastra(){
        // $sastra = Sastra::all();
        $getuser = DB::table('users')
                        ->select('*')
                        ->where('users.nama_user', '=', session()->get('nama_user'))
                        ->get();

        return view('user.tambahsastra', ['getuser'=> $getuser]);
    }

    public function tambahSastra(Request $request){
        $request->validate([
            'id_user' => 'required',
            'judul_sastra' => 'required',
            'nama_user' => 'required',
            'email' => 'required',
            'file_sastra' => 'required|mimes:pdf|max:10048',
            // 'status' => 'required'
        ]);

        if ($request->hasFile('file_sastra')) {
            $request->validate([
                'file_sastra' => 'required|mimes:pdf|max:10048',
            ]);

            $file= $request->file('file_sastra');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> move(public_path('/file_sastra'), $filename);

        }

        $daftarSastra = new Sastra([
            'id_user' => $request->id_user,
            'judul_sastra' => $request->judul_sastra,
            'nama_user' => $request->nama_user,
            'email' => $request->email,
            'file_sastra' => $filename,
            // 'status' => $request->status
        ]);
        $daftarSastra->save();

        return redirect('/user/sastra')->with('succes', 'Data Karya Sastra Berhasil Ditambahkan !!!');
    }

    public function detailKaryaSastraUser($sastra){
        $detailKaryaSastra = DB::table('sastra')
                            ->leftJoin('users', 'sastra.id_user', '=', 'users.id_user')
                            ->select('sastra.*', 'users.id_user')
                            ->where('sastra.id_sastra', $sastra)
                            ->get();

        return view('user.detailsastra', ['detailKaryaSastra'=> $detailKaryaSastra]);
    }

    public function deleteSastra($sastra){
            $oldfile_sastra = DB::table('sastra')
                                ->select('sastra.*')
                                ->where('id_sastra', $sastra)
                                ->get();
            // dd($oldfile_tulis);
            $oldfilepath = public_path('file_sastra\\'.$oldfile_sastra[0]->file_sastra);
            if(File::exists($oldfilepath)){
                File::delete($oldfilepath);
            }

        DB::table('sastra')
            ->where('id_sastra',$sastra)
            ->delete();
        return redirect('/user/sastra')->with('success', 'Karya Sastra Berhasil di hapus!');
    }
}
