<?php

namespace App\Http\Controllers;

use App\Models\Sastra;
use App\Models\Tulis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RekapController extends Controller
{
    //
    public function dashboardRekap(){
        //data kegiatan diterima
        $dataSastraDiterima = DB::table('sastra')
                                    ->where('status', 'Diterima')
                                    ->get();

        $dataSastraPending = DB::table('sastra')
                                    ->where('status', 'Pending')
                                    ->get();

        $dataSastraDitolak = DB::table('sastra')
                                    ->where('status', 'Ditolak')
                                    ->get();

        $dataTulisDiterima = DB::table('tulis')
                                    ->where('status', 'Diterima')
                                    ->get();
                                
        $dataTulisPending = DB::table('tulis')
                                    ->where('status', 'Pending')
                                    ->get();
        
        $dataTulisDitolak = DB::table('tulis')
                                    ->where('status', 'Ditolak')
                                    ->get();

        return view('admin.rekap', ['dataSastraDiterima'=> $dataSastraDiterima, 'dataSastraPending'=>$dataSastraPending, 'dataSastraDitolak'=>$dataSastraDitolak,
                                     'dataTulisDiterima'=> $dataTulisDiterima, 'dataTulisPending'=> $dataTulisPending, 'dataTulisDitolak'=> $dataTulisDitolak]);
        
    }
}
