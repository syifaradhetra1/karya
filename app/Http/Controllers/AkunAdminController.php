<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AkunAdminController extends Controller
{
    //
    public function showLogin(){
        return view('admin.login');
    }
    public function adminLogin(Request $request){
        $request->validate([
            'username'=>'required',
            'password'=>'required',
        ]);

        $datalogadmin = Admin::where('username', $request->username)
                                ->where('password', $request->password)
                                ->get();
        
        if(count($datalogadmin)<1){
            // return response()->json('disiini');
            
            return redirect('admin/login')->with('pesanlogin', 'Periksa Username dan Password Anda !');
        }else{
            $request->session()->put('nama_admin',$datalogadmin[0]['nama_admin']);
            // return response()->json($datalogadmin[0]['nama_admin']);
            return redirect('admin/index')->with('pesanlogin', 'Anda Berhasil Login Sebagai Admin');


        }
    }


    public function adminLogout(Request $request){
        $request->session()->forget('nama_admin');
        return redirect('admin/login')->with('logedout', 'Anda telah log out dari Admin');
        // return response()->json('admin berhasil logout');
    }
}
