<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AkunAdminController;
use App\Http\Controllers\AkunUserController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\RekapController;
use App\Http\Controllers\SastraController;
use App\Http\Controllers\TulisController;
use App\Models\Sastra;
use PHPUnit\Framework\Attributes\Group;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('login');
});

Route::group(['prefix' => 'admin'], function()
{
    Route::get('/login', [AkunAdminController::class, 'showLogin']);
    Route::post('/login', [AkunAdminController::class,'adminLogin']);
    Route::get('/logout', [AkunAdminController::class,'adminLogout']);

    Route::get('/index', [IndexController::class,'dashboardAdmin'])->middleware('adminmiddleware'); 

    Route::get('/tulis', [TulisController::class,'daftarKaryaTulis'])->middleware('adminmiddleware'); 
    Route::get('/tulis/detail/{tulis}', [TulisController::class,'detailKaryaTulis'])->middleware('adminmiddleware'); 
    Route::get('/tulis/{tulis}/edit', [TulisController::class,'admEditTulis'])->middleware('adminmiddleware'); 
    Route::patch('/tulis/{tulis}', [TulisController::class,'admUpdateTulis'])->middleware('adminmiddleware'); 

    Route::get('/sastra', [SastraController::class,'daftarKaryaSastra'])->middleware('adminmiddleware'); 
    Route::get('/sastra/detail/{sastra}', [SastraController::class,'detailKaryaSastra'])->middleware('adminmiddleware'); 
    Route::get('/sastra/{sastra}/edit', [SastraController::class,'admEditSastra'])->middleware('adminmiddleware');
    Route::patch('/sastra/{sastra}', [SastraController::class, 'admUpdateSastra'])->middleware('adminmiddleware');


    Route::get('/rekap', [RekapController::class,'dashboardRekap'])->middleware('adminmiddleware');     
});

Route::group(['prefix' => 'user'], function(){

    Route::get('/login', [AkunUserController::class, 'showLogin']);
    Route::post('/login', [AkunUserController::class,'userLogin']);
    Route::get('/logout', [AkunUserController::class,'userLogout']);

    Route::get('/index', [IndexController::class,'dashboardUser'])->middleware('usermiddleware'); 

    Route::get('/sastra', [SastraController::class,'daftarSastra'])->middleware('usermiddleware');
    Route::get('/sastra/detail/{sastra}', [SastraController::class,'detailKaryaSastraUser'])->middleware('adminmiddleware'); 
    Route::get('/sastra/create', [SastraController::class, 'createSastra'])->middleware('usermiddleware');
    Route::post('/sastra', [SastraController::class, 'tambahSastra'])->middleware('usermiddleware');
    Route::delete('/sastra/{sastra}', [SastraController::class, 'deleteSastra'])->middleware('usermiddleware');

    Route::get('/tulis', [TulisController::class, 'daftarTulis'])->middleware('usermiddleware');
    Route::get('/tulis/detail/{tulis}', [TulisController::class,'detailKaryaTulisUser'])->middleware('usermiddleware');
    Route::get('/tulis/create', [TulisController::class, 'createTulis'])->middleware('usermiddleware');
    Route::post('/tulis', [TulisController::class, 'tambahTulis'])->middleware('usermiddleware');
    Route::delete('/tulis/{tulis}', [TulisController::class, 'deleteTulis'])->middleware('usermiddleware');
    

});