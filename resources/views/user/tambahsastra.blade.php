<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>KARYA</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="{{asset('theme/images/favicon.ico')}}" />
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/bootstrap.min.css')}}">
      <!-- Chart list Js -->
      <link rel="stylesheet" href="{{asset('theme/js/chartist/chartist.min.css')}}">
      <!-- Typography CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/typography.css')}}">
      <!-- Style CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/style.css')}}">
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/responsive.css')}}">

      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">

   </head>
   <body>
      <!-- Wrapper Start -->
      <div class="wrapper">
         <!-- Sidebar  -->
         <div class="iq-sidebar">
            <div class="iq-sidebar-logo d-flex justify-content-between">
               <a href="{{url('user/index')}}">
               <div class="iq-light-logo">
                  <div class="iq-light-logo">
                     <img src="{{asset('images/logo.gif')}}" class="img-fluid" alt="">
                   </div>
                     <div class="iq-dark-logo">
                        <img src="{{asset('images/logo-dark.gif')}}" class="img-fluid" alt="">
                     </div>
               </div>
               <div class="iq-dark-logo">
                  <img src="{{asset('images/logo-dark.gif')}}" class="img-fluid" alt="">
               </div>
               <span>KARYA</span>
               </a>
            </div>
            <div id="sidebar-scrollbar">
               <nav class="iq-sidebar-menu">
                  <ul id="iq-sidebar-toggle" class="iq-menu">
                     <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Home</span></li>
                     <li>
                        <a href="{{url('user/index')}}" class="iq-waves-effect"><i class="ri-home-4-line"></i><span>Dashboard</span></a>
                     </li>
                     <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Apps</span></li>
                     
                     <li><a href="{{url('user/tulis')}}" class="iq-waves-effect" aria-expanded="false"><i class="ri-profile-line"></i><span>Karya Tulis</span></a></li>
                     <li class="active"><a href="{{url('user/sastra')}}" class="iq-waves-effect" aria-expanded="false"><i class="ri-stack-line"></i></i><span>Karya Sastra</span></a></li>
                  </ul>
               </nav>
               <div class="p-3"></div>
            </div>
         </div>
         <!-- TOP Nav Bar -->
         <div class="iq-top-navbar">
            <div class="iq-navbar-custom">
               <div class="iq-sidebar-logo">
                  <div class="top-logo">
                     <a href="{{url('user/index')}}" class="logo">
                     <div class="iq-light-logo">
                  <img src="{{asset('theme/images/logo.gif')}}" class="img-fluid" alt="">
               </div>
               <div class="iq-dark-logo">
                  <img src="{{asset('theme/images/logo-dark.gif')}}" class="img-fluid" alt="">
               </div>
                     <span>KARYA</span>
                     </a>
                  </div>
               </div>
               <nav class="navbar navbar-expand-lg navbar-light p-0">
                  <div class="navbar-left">                  
                     <div class="iq-search-bar d-none d-md-block">
                        <h4>Tulisan Karya</h4>
                     </div>
                  </div>
                  <div class="iq-menu-bt align-self-center">
                     <div class="wrapper-menu">
                        <div class="main-circle"><i class="ri-arrow-left-s-line"></i></div>
                        <div class="hover-circle"><i class="ri-arrow-right-s-line"></i></div>
                     </div>
                  </div>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav ml-auto navbar-list">
                     </ul>
                  </div>
                  <ul class="navbar-list">
                     <li>
                        <a href="#" class="search-toggle iq-waves-effect d-flex align-items-center bg-primary rounded">
                           <img src="{{asset('theme/images/user/1.jpg')}}" class="img-fluid rounded mr-3" alt="user">
                           <div class="caption">
                              <h6 class="mb-0 line-height text-white">{{ session()->get('nama_user') }}</h6>
                              <span class="font-size-12 text-white">Available</span>
                           </div>
                        </a>
                        <div class="iq-sub-dropdown iq-user-dropdown">
                           <div class="iq-card shadow-none m-0">
                              <div class="iq-card-body p-0 ">
                                 <div class="bg-primary p-3">
                                    <h5 class="mb-0 text-white line-height">{{ session()->get('nama_user') }}</h5>
                                    <span class="text-white font-size-12">Available</span>
                                 </div>
                                 <div class="d-inline-block w-100 text-center p-3">
                                    <a class="btn btn-primary dark-btn-primary" href="/user/logout" role="button">Sign out<i class="ri-login-box-line ml-2"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </li>
                  </ul>
               </nav>
               

            </div>
         </div>
         <!-- TOP Nav Bar END -->
         <!-- Page Content  -->
         <div id="content-page" class="content-page">
            <div class="container-fluid">
               @if (session()->has('sukseslogin'))
                     <div class="alert alert-success alert-dismissible fade show" role="alert" id="autoalert">
                        {{ session('sukseslogin') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
               @endif
               
                <div class="row">
                    <div class="col-sm-12">
                        <div class="iq-card">
                            <div class="iq-card-header d-flex justify-content-between">
                                <div class="iq-header-title">
                                    <h4 class="card-title">Tambah Karya Sastra</h4>
                                </div>
                            </div>
                            <div class="iq-card-body">
                                <form action="{{ url('user/sastra') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInputText1">Nama </label>
                                        <input type="text" class="form-control" value="{{ $getuser[0]->nama_user }}" placeholder="Nama" id="nama_user" name="nama_user" readonly>
                                    </div>

                                        <input type="text" class="form-control" value="{{ $getuser[0]->id_user }}" placeholder="Nama" id="id_user" name="id_user" hidden>

                                    <div class="form-group">
                                        <label for="exampleInputText1">Judul </label>
                                        <input type="text" class="form-control @error('judul_sastra') is-invalid @enderror" id="judul_sastra" name="judul_sastra" placeholder="Judul">
                                          @error('judul_sastra')
                                          <div class="invalid-feedback">
                                                {{ $message }}
                                          </div>
                                          @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputText1">Email </label>
                                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Email ">
                                          @error('email')
                                          <div class="invalid-feedback">
                                                {{ $message }}
                                          </div>
                                          @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputText1">Upload File </label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input @error('file_sastra') is-invalid @enderror" id="file_sastra" name="file_sastra">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                             @error('file_sastra')
                                             <div class="invalid-feedback">
                                                   {{ $message }}
                                             </div>
                                             @enderror
                                        </div>
                                        <span>Upload dalam bentuk PDF | < 10MB </span>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Tambah Karya Sastra</button>
                                    <a class="btn iq-bg-danger text-decoration-none" data-toggle="tooltip" title=""  href="{{ url('user/sastra') }}">Batal</i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>                                         
            </div>
              
            
         </div>

      <!-- Wrapper END -->

      <script src="{{asset('theme/js/jquery.min.js')}}"></script>
      
      <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
      <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
      <script>
         $(document).ready( function () {
            $('#table_id').DataTable({
               "scrollX" : false
            });
         } );
      </script>
   
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      
      <!-- <script src="{{asset('theme/js/jquery.min.js')}}"></script> -->
      
      <script src="{{asset('theme/js/popper.min.js')}}"></script>
      <script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
      <!-- Appear JavaScript -->
      <script src="{{asset('theme/js/jquery.appear.js')}}"></script>
      <!-- Countdown JavaScript -->
      <script src="{{asset('theme/js/countdown.min.js')}}"></script>
      <!-- Counterup JavaScript -->
      <script src="{{asset('theme/js/waypoints.min.js')}}"></script>
      <script src="{{asset('theme/js/jquery.counterup.min.js')}}"></script>
      <!-- Wow JavaScript -->
      <script src="{{asset('theme/js/wow.min.js')}}"></script>
      <!-- Apexcharts JavaScript -->
      <script src="{{asset('theme/js/apexcharts.js')}}"></script>
      <!-- Slick JavaScript -->
      <script src="{{asset('theme/js/slick.min.js')}}"></script>
      <!-- Select2 JavaScript -->
      <script src="{{asset('theme/js/select2.min.js')}}"></script>
      <!-- Magnific Popup JavaScript -->
      <script src="{{asset('theme/js/jquery.magnific-popup.min.js')}}"></script>
      <!-- Smooth Scrollbar JavaScript -->
      <script src="{{asset('theme/js/smooth-scrollbar.js')}}"></script>
      <!-- lottie JavaScript -->
      <script src="{{asset('theme/js/lottie.js')}}"></script>
      <!-- am core JavaScript -->
      <script src="{{asset('theme/js/core.js')}}"></script>
      <!-- am charts JavaScript -->
      <script src="{{asset('theme/js/charts.js')}}"></script>
      <!-- am animated JavaScript -->
      <script src="{{asset('theme/js/animated.js')}}"></script>
      <!-- am kelly JavaScript -->
      <script src="{{asset('theme/js/kelly.js')}}"></script>
      <!-- Morris JavaScript -->
      <script src="{{asset('theme/js/morris.js')}}"></script>
      <!-- am maps JavaScript -->
      <script src="{{asset('theme/js/maps.js')}}"></script>
      <!-- am worldLow JavaScript -->
      <script src="{{asset('theme/js/worldLow.js')}}"></script>
      <!-- ChartList Js -->
      <script src="{{asset('theme/js/chartist/chartist.min.js')}}"></script>
      <!-- Chart Custom JavaScript -->
      <script async src="{{asset('theme/js/chart-custom.js')}}"></script>
      <!-- Custom JavaScript -->
      <script src="{{asset('theme/js/custom.js')}}"></script>

      <script>
         $("#autoalert").fadeTo(2000, 500).slideUp(500, function(){
            $("#autoalert").slideUp(500);
         })
      </script>
   </body>
</html>
