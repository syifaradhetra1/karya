<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>KARYA</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="{{asset('theme/images/favicon.ico')}}" />
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/bootstrap.min.css')}}">
      <!-- Chart list Js -->
      <link rel="stylesheet" href="{{asset('theme/js/chartist/chartist.min.css')}}">
      <!-- Typography CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/typography.css')}}">
      <!-- Style CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/style.css')}}">
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/responsive.css')}}">
   </head>
   <body class="bg-white">
        <!-- Sign in Start -->
        <section class="sign-in-page bg-white">
            <div class="container bg-white p-0"">
                <div class="row no-gutters">
                    <div class="col-sm-2 align-self-center">

                    </div>
                    <div class="col-sm-8 align-self-center">
                        <div class="sign-in-from" style="padding-top: 15%;">

                        @if (session()->has('pesanlogin'))
                            <div class="alert alert-warning alert-dismissible fade show" role="alert" id="autoalert">
                                {{ session('pesanlogin') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                            <h1 class="mb-0 dark-signin">Sign in</h1>
                            <h5>Masuk Sebagai <span style="font-weight: bold;" >User</span> .</h5>
                            <form class="mt-4" action="{{ url('user/login') }}" method="post" >
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputText1">Username</label>
                                    <input type="text" class="form-control mb-0" id="username" name="username" placeholder="Username" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control mb-0" id="password" name="password" placeholder="Password" required>
                                </div>
                                <div class="d-inline-block w-100">
                                    <button type="submit" class="btn btn-primary float-right">Sign in</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Sign in END -->
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="{{asset('theme/js/jquery.min.js')}}"></script>
      
      <script src="{{asset('theme/js/popper.min.js')}}"></script>
      <script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
      <!-- Appear JavaScript -->
      <script src="{{asset('theme/js/jquery.appear.js')}}"></script>
      <!-- Countdown JavaScript -->
      <script src="{{asset('theme/js/countdown.min.js')}}"></script>
      <!-- Counterup JavaScript -->
      <script src="{{asset('theme/js/waypoints.min.js')}}"></script>
      <script src="{{asset('theme/js/jquery.counterup.min.js')}}"></script>
      <!-- Wow JavaScript -->
      <script src="{{asset('theme/js/wow.min.js')}}"></script>
      <!-- Apexcharts JavaScript -->
      <script src="{{asset('theme/js/apexcharts.js')}}"></script>
      <!-- Slick JavaScript -->
      <script src="{{asset('theme/js/slick.min.js')}}"></script>
      <!-- Select2 JavaScript -->
      <script src="{{asset('theme/js/select2.min.js')}}"></script>
      <!-- Magnific Popup JavaScript -->
      <script src="{{asset('theme/js/jquery.magnific-popup.min.js')}}"></script>
      <!-- Smooth Scrollbar JavaScript -->
      <script src="{{asset('theme/js/smooth-scrollbar.js')}}"></script>
      <!-- lottie JavaScript -->
      <script src="{{asset('theme/js/lottie.js')}}"></script>
      <!-- am core JavaScript -->
      <script src="{{asset('theme/js/core.js')}}"></script>
      <!-- am charts JavaScript -->
      <script src="{{asset('theme/js/charts.js')}}"></script>
      <!-- am animated JavaScript -->
      <script src="{{asset('theme/js/animated.js')}}"></script>
      <!-- am kelly JavaScript -->
      <script src="{{asset('theme/js/kelly.js')}}"></script>
      <!-- Morris JavaScript -->
      <script src="{{asset('theme/js/morris.js')}}"></script>
      <!-- am maps JavaScript -->
      <script src="{{asset('theme/js/maps.js')}}"></script>
      <!-- am worldLow JavaScript -->
      <script src="{{asset('theme/js/worldLow.js')}}"></script>
      <!-- ChartList Js -->
      <script src="{{asset('theme/js/chartist/chartist.min.js')}}"></script>
      <!-- Chart Custom JavaScript -->
      <script async src="{{asset('theme/js/chart-custom.js')}}"></script>
      <!-- Custom JavaScript -->
      <script src="{{asset('theme/js/custom.js')}}"></script>

      <script>
         $("#autoalert").fadeTo(2000, 500).slideUp(500, function(){
            $("#autoalert").slideUp(500);
         })
      </script>
   </body>
</html>