<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>KARYA</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="{{asset('theme/images/favicon.ico')}}" />
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/bootstrap.min.css')}}">
      <!-- Chart list Js -->
      <link rel="stylesheet" href="{{asset('theme/js/chartist/chartist.min.css')}}">
      <!-- Typography CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/typography.css')}}">
      <!-- Style CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/style.css')}}">
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/responsive.css')}}">
   </head>
   <body>

        <section class="sign-in-page">
            <div class="container p-0 bg-white">
                <div class="row no-gutters">
                    <div class="col-sm-6 align-self-center">
                        <div class="sign-in-from">
                                    <h4 class="mt-3 mb-0">Selamat Datang di Kirim Tulisan Karya</h4>
                                    <p>Masuk Untuk Melanjutkan.</p>
                            <div class="iq-card-header d-flex justify-content-start">
                                 <span class="float-right mb-3 mr-2">
                                    <h4><a href="{{url('/login')}}" class="badge badge-primary text-decoration-none"><span> Login</span></a></li></a></h4>
                                 </span>
                              </div>
                        </div>
                    </div>
                    <div class="col-sm-6 text-center">
                        <div class="sign-in-detail text-white">
                            <h4 class="mb-5 text-white">KARYA</h4>
                            <div class="slick-slider11">
                                <div class="item">
                                    <img src="{{asset('theme/images/page-img/Data4.png')}}" class="img-fluid" alt="logo">
                                </div>
                                <div class="item">
                                    <img src="{{asset('theme/images/page-img/Data3.png')}}" class="img-fluid" alt="logo">
                                </div>
                                <div class="item">
                                    <img src="{{asset('theme/images/page-img/Data6.png')}}" class="img-fluid" alt="logo">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

     
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      
      <script src="{{asset('theme/js/jquery.min.js')}}"></script>
      
      <script src="{{asset('theme/js/popper.min.js')}}"></script>
      <script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
      <!-- Appear JavaScript -->
      <script src="{{asset('theme/js/jquery.appear.js')}}"></script>
      <!-- Countdown JavaScript -->
      <script src="{{asset('theme/js/countdown.min.js')}}"></script>
      <!-- Counterup JavaScript -->
      <script src="{{asset('theme/js/waypoints.min.js')}}"></script>
      <script src="{{asset('theme/js/jquery.counterup.min.js')}}"></script>
      <!-- Wow JavaScript -->
      <script src="{{asset('theme/js/wow.min.js')}}"></script>
      <!-- Apexcharts JavaScript -->
      <script src="{{asset('theme/js/apexcharts.js')}}"></script>
      <!-- Slick JavaScript -->
      <script src="{{asset('theme/js/slick.min.js')}}"></script>
      <!-- Select2 JavaScript -->
      <script src="{{asset('theme/js/select2.min.js')}}"></script>
      <!-- Magnific Popup JavaScript -->
      <script src="{{asset('theme/js/jquery.magnific-popup.min.js')}}"></script>
      <!-- Smooth Scrollbar JavaScript -->
      <script src="{{asset('theme/js/smooth-scrollbar.js')}}"></script>
      <!-- lottie JavaScript -->
      <script src="{{asset('theme/js/lottie.js')}}"></script>
      <!-- am core JavaScript -->
      <script src="{{asset('theme/js/core.js')}}"></script>
      <!-- am charts JavaScript -->
      <script src="{{asset('theme/js/charts.js')}}"></script>
      <!-- am animated JavaScript -->
      <script src="{{asset('theme/js/animated.js')}}"></script>
      <!-- am kelly JavaScript -->
      <script src="{{asset('theme/js/kelly.js')}}"></script>
      <!-- Morris JavaScript -->
      <script src="{{asset('theme/js/morris.js')}}"></script>
      <!-- am maps JavaScript -->
      <script src="{{asset('theme/js/maps.js')}}"></script>
      <!-- am worldLow JavaScript -->
      <script src="{{asset('theme/js/worldLow.js')}}"></script>
      <!-- ChartList Js -->
      <script src="{{asset('theme/js/chartist/chartist.min.js')}}"></script>
      <!-- Chart Custom JavaScript -->
      <script async src="{{asset('theme/js/chart-custom.js')}}"></script>
      <!-- Custom JavaScript -->
      <script src="{{asset('theme/js/custom.js')}}"></script>
   </body>
</html>
