<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>karya</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="{{asset('theme/images/favicon.ico')}}" />
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/bootstrap.min.css')}}">
      <!-- Chart list Js -->
      <link rel="stylesheet" href="{{asset('theme/js/chartist/chartist.min.css')}}">
      <!-- Typography CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/typography.css')}}">
      <!-- Style CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/style.css')}}">
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/responsive.css')}}">
   </head>
   <body style="background: rgba(130, 122, 243, 1) !important;">

        <div class="wrapper" >
            <div class=" container-fluid">
                <div class="iq-comingsoon p-0">
                    <div class="row justify-content-center">
                        <div class="col-sm-12 text-center">
                            <div class="iq-comingsoon-info">    
                                <img src="{{asset('theme/images/page-img/Data11.png')}}" class="img-fluid w-25" alt=""> 
                                <h2 class="mb-0 text-white">Masuk Dan Lanjutkan Sebagai</h2>
                                <a class="btn btn-primary mt-3" href="{{url('admin/login')}}"><i class="ri-user-3-line"></i></i>Admin</a>                                                   
                                <a class="btn btn-primary mt-3" href="{{url('user/login')}}"><i class="ri-user-3-line"></i></i>User</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      
      <script src="{{asset('theme/js/jquery.min.js')}}"></script>
      
      <script src="{{asset('theme/js/popper.min.js')}}"></script>
      <script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
      <!-- Appear JavaScript -->
      <script src="{{asset('theme/js/jquery.appear.js')}}"></script>
      <!-- Countdown JavaScript -->
      <script src="{{asset('theme/js/countdown.min.js')}}"></script>
      <!-- Counterup JavaScript -->
      <script src="{{asset('theme/js/waypoints.min.js')}}"></script>
      <script src="{{asset('theme/js/jquery.counterup.min.js')}}"></script>
      <!-- Wow JavaScript -->
      <script src="{{asset('theme/js/wow.min.js')}}"></script>
      <!-- Apexcharts JavaScript -->
      <script src="{{asset('theme/js/apexcharts.js')}}"></script>
      <!-- Slick JavaScript -->
      <script src="{{asset('theme/js/slick.min.js')}}"></script>
      <!-- Select2 JavaScript -->
      <script src="{{asset('theme/js/select2.min.js')}}"></script>
      <!-- Magnific Popup JavaScript -->
      <script src="{{asset('theme/js/jquery.magnific-popup.min.js')}}"></script>
      <!-- Smooth Scrollbar JavaScript -->
      <script src="{{asset('theme/js/smooth-scrollbar.js')}}"></script>
      <!-- lottie JavaScript -->
      <script src="{{asset('theme/js/lottie.js')}}"></script>
      <!-- am core JavaScript -->
      <script src="{{asset('theme/js/core.js')}}"></script>
      <!-- am charts JavaScript -->
      <script src="{{asset('theme/js/charts.js')}}"></script>
      <!-- am animated JavaScript -->
      <script src="{{asset('theme/js/animated.js')}}"></script>
      <!-- am kelly JavaScript -->
      <script src="{{asset('theme/js/kelly.js')}}"></script>
      <!-- Morris JavaScript -->
      <script src="{{asset('theme/js/morris.js')}}"></script>
      <!-- am maps JavaScript -->
      <script src="{{asset('theme/js/maps.js')}}"></script>
      <!-- am worldLow JavaScript -->
      <script src="{{asset('theme/js/worldLow.js')}}"></script>
      <!-- ChartList Js -->
      <script src="{{asset('theme/js/chartist/chartist.min.js')}}"></script>
      <!-- Chart Custom JavaScript -->
      <script async src="{{asset('theme/js/chart-custom.js')}}"></script>
      <!-- Custom JavaScript -->
      <script src="{{asset('theme/js/custom.js')}}"></script>
   </body>
</html>
