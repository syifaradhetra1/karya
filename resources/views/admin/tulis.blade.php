<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>KARYA</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="{{asset('theme/images/favicon.ico')}}" />
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/bootstrap.min.css')}}">
      <!-- Chart list Js -->
      <link rel="stylesheet" href="{{asset('theme/js/chartist/chartist.min.css')}}">
      <!-- Typography CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/typography.css')}}">
      <!-- Style CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/style.css')}}">
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="{{asset('theme/css/responsive.css')}}">

      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">

   </head>
   <body>
      <!-- Wrapper Start -->
      <div class="wrapper">
         <!-- Sidebar  -->
         <div class="iq-sidebar">
            <div class="iq-sidebar-logo d-flex justify-content-between">
               <a href="{{url('admin/index')}}">
               <div class="iq-light-logo">
                  <div class="iq-light-logo">
                     <img src="{{asset('images/logo.gif')}}" class="img-fluid" alt="">
                   </div>
                     <div class="iq-dark-logo">
                        <img src="{{asset('images/logo-dark.gif')}}" class="img-fluid" alt="">
                     </div>
               </div>
               <div class="iq-dark-logo">
                  <img src="{{asset('images/logo-dark.gif')}}" class="img-fluid" alt="">
               </div>
               <span>KARYA</span>
               </a>
            </div>
            <div id="sidebar-scrollbar">
               <nav class="iq-sidebar-menu">
                  <ul id="iq-sidebar-toggle" class="iq-menu">
                     <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Home</span></li>
                     <li>
                        <a href="{{url('admin/index')}}" class="iq-waves-effect"><i class="ri-home-4-line"></i><span>Dashboard</span></a>
                     </li>
                     <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Apps</span></li>
                     
                     <li  class="active"><a href="{{url('admin/tulis')}}" class="iq-waves-effect" aria-expanded="false"><i class="ri-profile-line"></i><span>Karya Tulis</span></a></li>
                     <li><a href="{{url('admin/sastra')}}" class="iq-waves-effect" aria-expanded="false"><i class="ri-stack-line"></i></i><span>Karya Sastra</span></a></li>
                     <li><a href="{{url('admin/rekap')}}" class="iq-waves-effect" aria-expanded="false"><i class="ri-stack-line"></i></i><span>Rekap</span></a></li>
                  </ul>
               </nav>
               <div class="p-3"></div>
            </div>
         </div>
         <!-- TOP Nav Bar -->
         <div class="iq-top-navbar">
            <div class="iq-navbar-custom">
               <div class="iq-sidebar-logo">
                  <div class="top-logo">
                     <a href="{{url('admin/index')}}" class="logo">
                     <div class="iq-light-logo">
                  <img src="{{asset('theme/images/logo.gif')}}" class="img-fluid" alt="">
               </div>
               <div class="iq-dark-logo">
                  <img src="{{asset('theme/images/logo-dark.gif')}}" class="img-fluid" alt="">
               </div>
                     <span>semilir</span>
                     </a>
                  </div>
               </div>
               <nav class="navbar navbar-expand-lg navbar-light p-0">
                  <div class="navbar-left">                  
                     <div class="iq-search-bar d-none d-md-block">
                        <h4>Tulisan Karya</h4>
                     </div>
                  </div>
                  <div class="iq-menu-bt align-self-center">
                     <div class="wrapper-menu">
                        <div class="main-circle"><i class="ri-arrow-left-s-line"></i></div>
                        <div class="hover-circle"><i class="ri-arrow-right-s-line"></i></div>
                     </div>
                  </div>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav ml-auto navbar-list">
                     </ul>
                  </div>
                  <ul class="navbar-list">
                     <li>
                        <a href="#" class="search-toggle iq-waves-effect d-flex align-items-center bg-primary rounded">
                           <img src="{{asset('theme/images/user/1.jpg')}}" class="img-fluid rounded mr-3" alt="user">
                           <div class="caption">
                              <h6 class="mb-0 line-height text-white">{{ session()->get('nama_admin') }}</h6>
                              <span class="font-size-12 text-white">Available</span>
                           </div>
                        </a>
                        <div class="iq-sub-dropdown iq-user-dropdown">
                           <div class="iq-card shadow-none m-0">
                              <div class="iq-card-body p-0 ">
                                 <div class="bg-primary p-3">
                                    <h5 class="mb-0 text-white line-height">{{ session()->get('nama_admin') }}</h5>
                                    <span class="text-white font-size-12">Available</span>
                                 </div>
                                 <div class="d-inline-block w-100 text-center p-3">
                                    <a class="btn btn-primary dark-btn-primary" href="/admin/logout" role="button">Sign out<i class="ri-login-box-line ml-2"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </li>
                  </ul>
               </nav>
               

            </div>
         </div>
         <!-- TOP Nav Bar END -->
         <!-- Page Content  -->
         <div id="content-page" class="content-page">
            <div class="container-fluid">
               @if (session()->has('sukseslogin'))
                     <div class="alert alert-success alert-dismissible fade show" role="alert" id="autoalert">
                        {{ session('sukseslogin') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
               @endif
               <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12">
                     <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                           <div class="iq-header-title">
                              <h4 class="card-title"> Jumlah Pengajuan Tulisan Karya Tulis</h4>
                           </div>
                        </div>
                        <div class="iq-card-header d-flex justify-content-end">
                           <span class="float-right mb-3 mr-2">
                              <!-- <h4><a href="{{url('mahasiswa/kegiatan/create')}}" class="badge badge-info text-decoration-none"><i class="ri-add-line"></i><span> Tambah Kegiatan</span></a></li></a></h4> -->
                           </span>
                        </div>
                        <div class="iq-card-body">
                           <div id="table" class="">
                              <table class="table" id="table_id" style="display: inline-table;">
                                 <thead>
                                    <tr>
                                       <th>Judul</th>
                                       <th>Nama</th>
                                       <th>Status</th>
                                       <th class="text-center">Aksi</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach ($daftarKaryaTulis as $tulis)
                                    <tr>
                                       <td>{{$tulis->judul_tulis}}</td>
                                       <td>{{$tulis->nama_user}}</td>
                                       <td>{{$tulis->status}}</td>
                                       <td class="" >
                                          <div class="list-user-action text-center">
                                             <a class="iq-bg-primary text-decoration-none" href="/admin/tulis/detail/{{ $tulis->id_tulis }}"><i class="ri-eye-line"></i></a>
                                             <a class="iq-bg-primary text-decoration-none" href="/admin/tulis/{{ $tulis->id_tulis }}/edit"><i class="ri-pencil-line"></i></a>
                                          </div>
                                       </td>
                                    </tr>

                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>          
            </div>
              
            
         </div>

      <!-- Wrapper END -->

      <script src="{{asset('theme/js/jquery.min.js')}}"></script>
      
      <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
      <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
      <script>
         $(document).ready( function () {
            $('#table_id').DataTable({
               "scrollX" : false
            });
         } );
      </script>
   
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      
      <!-- <script src="{{asset('theme/js/jquery.min.js')}}"></script> -->
      
      <script src="{{asset('theme/js/popper.min.js')}}"></script>
      <script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
      <!-- Appear JavaScript -->
      <script src="{{asset('theme/js/jquery.appear.js')}}"></script>
      <!-- Countdown JavaScript -->
      <script src="{{asset('theme/js/countdown.min.js')}}"></script>
      <!-- Counterup JavaScript -->
      <script src="{{asset('theme/js/waypoints.min.js')}}"></script>
      <script src="{{asset('theme/js/jquery.counterup.min.js')}}"></script>
      <!-- Wow JavaScript -->
      <script src="{{asset('theme/js/wow.min.js')}}"></script>
      <!-- Apexcharts JavaScript -->
      <script src="{{asset('theme/js/apexcharts.js')}}"></script>
      <!-- Slick JavaScript -->
      <script src="{{asset('theme/js/slick.min.js')}}"></script>
      <!-- Select2 JavaScript -->
      <script src="{{asset('theme/js/select2.min.js')}}"></script>
      <!-- Magnific Popup JavaScript -->
      <script src="{{asset('theme/js/jquery.magnific-popup.min.js')}}"></script>
      <!-- Smooth Scrollbar JavaScript -->
      <script src="{{asset('theme/js/smooth-scrollbar.js')}}"></script>
      <!-- lottie JavaScript -->
      <script src="{{asset('theme/js/lottie.js')}}"></script>
      <!-- am core JavaScript -->
      <script src="{{asset('theme/js/core.js')}}"></script>
      <!-- am charts JavaScript -->
      <script src="{{asset('theme/js/charts.js')}}"></script>
      <!-- am animated JavaScript -->
      <script src="{{asset('theme/js/animated.js')}}"></script>
      <!-- am kelly JavaScript -->
      <script src="{{asset('theme/js/kelly.js')}}"></script>
      <!-- Morris JavaScript -->
      <script src="{{asset('theme/js/morris.js')}}"></script>
      <!-- am maps JavaScript -->
      <script src="{{asset('theme/js/maps.js')}}"></script>
      <!-- am worldLow JavaScript -->
      <script src="{{asset('theme/js/worldLow.js')}}"></script>
      <!-- ChartList Js -->
      <script src="{{asset('theme/js/chartist/chartist.min.js')}}"></script>
      <!-- Chart Custom JavaScript -->
      <script async src="{{asset('theme/js/chart-custom.js')}}"></script>
      <!-- Custom JavaScript -->
      <script src="{{asset('theme/js/custom.js')}}"></script>

      <script>
         $("#autoalert").fadeTo(2000, 500).slideUp(500, function(){
            $("#autoalert").slideUp(500);
         })
      </script>
   </body>
</html>
